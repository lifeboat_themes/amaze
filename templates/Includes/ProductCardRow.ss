<a href="$Link" class="product-card">
    <div class="image">
        <% if $Image %>
            <picture>
                <source srcset="$Image.Pad(640,640).AbsoluteLink" media="(-webkit-min-device-pixel-ratio: 2)" />
                <img src="$Image.Pad(320,320).AbsoluteLink" alt="$Title" width="320" height="320" loading="lazy" />
            </picture>
        <% else %>
            <img src="$SiteSettings.Logo.Pad(320,320).AbsoluteLink" alt="No Image Found" />
        <% end_if %>
        <% if $BestSellerIn.First.Rank > 0 && $BestSellerIn.First.Rank < 6 %>
            <span class="best-seller-badge"><strong>Best Seller</strong></span>
        <% end_if %>
    </div>
    <div class="details">
        <span class="title">$Title.LimitCharacters(180)</span>
        <small class="summary">$Summary.LimitCharacters(200)</small>
        <span class="price">
            <% if $MinPrice != $MaxPrice %>
                <small class="pulse">starting from</small>
            <% end_if %>
            $FormatPrice('MinPrice')
        </span>
    </div>
</a>