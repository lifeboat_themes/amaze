<div class="container">
    <div class="row responsive">
        <div class="c12">
            <section class="breadcrumbs">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>/</li>
                    <li>$Title</li>
                </ul>
            </section>
        </div>
    </div>
    <% if $URLSegment == 'brands' %>
        <% cached 'brands_list', $ThemeCacheID, $LastEdited, $ModelObjects('Collection').filter('Tags:PartialMatch', 'brand_page').max('LastEdited') %>
        <section class="titled-section standalone">
            <div class="section-items">
                <span class="section-title">
                    #Brands
                    <small>Discover all the major brands on $SiteSettings.Title</small>
                </span>
                <div class="row responsive">
                    <% loop $ModelObjects('Collection').filter('Tags:PartialMatch', 'brand_page').sort('Title', 'ASC') %>
                        <div class="c2 m">
                            <a href="$Link" class="brand-tile">
                                <img src="$Image.Pad(250,250).Link" alt="$MetaDescription">
                                <span>$Title</span>
                            </a>
                        </div>
                    <% end_loop %>
                </div>
            </div>
        </section>
        <% end_cached %>
    <% else_if $URLSegment == 'deals' %>
        <% cached 'deal_list', $ThemeCacheID, $ModelObjects('Product').max('LastEdited'), $ModelObjects('Product').count %>
            <div class="product-list collection">
                <% loop $ModelObjects('Product').filter('hasVariants', false).filter('Discounted:GreaterThan', 0.01).sort('(1-(Discounted/Price))', 'DESC').limit(100) %>
                        <div class="product-slide">
                        <% include ProductCardSquare %>
                        </div>
                <% end_loop %>
            </div>
        <% end_cached %>
    <% else %>
        <div class="row">
            <div class="c12">
                <h1>$Title</h1>
            </div>
        </div>
        <div class="row">
            <div class="c12">
                $Content
            </div>
        </div>
    <% end_if %>
</div>