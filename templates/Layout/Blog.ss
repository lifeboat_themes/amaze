<div class="container">
    <div class="row responsive">
        <div class="c12">
            <section class="breadcrumbs">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li>/</li>
                    <li>$Title</li>
                </ul>
            </section>
        </div>
    </div>
    <div class="row responsive article-list">
        <% loop $PaginatedPages(12) %>
            <div class="c4 m">
                <a href="$Link" class="article-card">
                    <img src="$Image.Fill(360,360).Link" alt="$Title">
                    <div class="row responsive">
                        <div class="c7 m"><strong>$Title</strong></div>
                        <div class="c5 m"><em>$Created.Format('d MMM yy')</em></div>
                    </div>
                </a>
            </div>
        <% end_loop %>
    </div>
    <div class="pagination">
        <% if $PaginatedPages(12).MoreThanOnePage %>
            <% with $PaginatedPages(12) %>
                <ul>
                    <% if $NotFirstPage %>
                        <li>
                            <a class="prev" href="$PrevLink" aria-label="Previous" tabindex="-1"
                               aria-disabled="true">
                                Prev
                            </a>
                        </li>
                    <% end_if %>
                    <% loop $PaginationSummary(2) %>
                        <% if $CurrentBool %>
                        <li><a href="#" class="active">$PageNum</a>
                        <% else %>
                            <% if $Link %>
                                <li><a href="$Link">$PageNum</a></li>
                            <% else %>
                                <li>...</li>
                            <% end_if %>
                        <% end_if %>
                    <% end_loop %>
                    <% if $NotLastPage %>
                        <li>
                            <a class="next" href="$NextLink" aria-label="Next">
                                Next
                            </a>
                        </li>
                    <% end_if %>
                </ul>
            <% end_with %>
        <% end_if %>
    </div>
</div>