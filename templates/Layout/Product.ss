<section class="product-top row responsive">
    <div class="gallery c6 row">
        <% cached 'product_thmumbs', $ThemeCacheID, $ID, $LastEdited, $ProductImages.count(), $ProductImages.max('LastEdited') %>
            <div class="thumbnails column hide-scroll"></div>
            <div class="images owl-carousel">
            <% loop $ProductImages %>
                <div class="slide-owl-wrap">
                    <picture data-id="$ID">
                        <source srcset="$Pad(1280,1280).AbsoluteLink" media="(min-width: 640px) and (-webkit-min-device-pixel-ratio: 2)" />
                        <source srcset="$Pad(640,640).AbsoluteLink" media="(min-width: 640px)"/>
                        <source srcset="$Pad(640,640).AbsoluteLink" media="(min-width: 320px) and (-webkit-min-device-pixel-ratio: 2) "/>
                        <img src="$Pad(320,320).AbsoluteLink" alt="$Top.Title - Image $Pos"
                             width="375" height="375" class="prod-img" data-zoom-image="$AbsoluteLink"
                             data-zoom-width="$getWidth" data-zoom-height="$getHeight" />
                    </picture>
                </div>
            <% end_loop %>
            </div>
        <% end_cached %>
        <p class="text-center"><small>Click to open expanded view</small></p>
        <div class="lightbox">
            <div class="content row">
                <a class="close-btn">X</a>
                <div class="lightbox-image"></div>
                <div class="right">
                    <h2>$Title</h2>
                    <p><small>$Summary</small></p>
                    <p>&nbsp;</p>
                    <div class="lightbox-thumbnails"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="details c6 no-gutters row responsive">
        <div class="c8">
            <h1>$Title</h1>
            <% if $SiteSettings.EnableReviews %>
                <% if $countReviews > 0 %>
                    <div class="rating row nw">
                        <i class="rating-stars-{$Rating}"></i>
                        <span>$countReviews reviews</span>
                    </div>
                <% else %>
                    <div class="rating row nw no-ratings">
                        <i class="rating-stars-0"></i>
                        <span><em>No ratings</em></span>
                    </div>
                <% end_if %>
            <% end_if %>
            <div class="product-bagdes">
                <% if $BestSellerIn.First.Rank > 0 && $BestSellerIn.First.Rank < 6 %>
                    <span class="best-seller-badge">
                        <strong>#{$BestSellerIn.First.Rank} Best Seller</strong>
                        <% with $BestSellerIn.First.Collection %>
                            in <a href="$Link">$Title</a>
                        <% end_with %>
                    </span>
                <% end_if %>
            </div>
            <div id="price-form" data-id="$ID" data-panel="info-panel"></div>
            <p class="summary">$Summary</p>
            <table class="metadata-list">
                <% if $SKU %><tr><th>SKU</th><td>$SKU</td></tr><% end_if %>
                <% loop $getMetaDataList %>
                    <tr><th>$Key</th><td>$Value</td></tr>
                <% end_loop %>
            </table>
        </div>
        <div class="c4">
            <div class="info-panel" id="info-panel">
                <div class="price"></div>
                <div class="text"></div>
                <div class="stock-availability"></div>

                <div class="row qty nw">
                    <label for="quantity">Quantity: </label>
                    <input type="number" step="1" min="1" class="quantity" id="quantity" value="1" />
                </div>
                <button type="button" class="add-cart-btn">Add To Cart</button>
                <a href="#" class="buy-now-btn">Buy Now</a>
                <div class="trust-info">
                    <a href="#" id="secure-btn">
                        <i class="icon-credit-cards-payment"></i>
                        Secure Transaction
                    </a>
                    <a href="#" id="return-btn">
                        <i class="icon-refund"></i>
                        Return Policy
                    </a>
                </div>
                <table class="metadata-list mobile">
                    <% if $SKU %><tr><th>SKU</th><td>$SKU</td></tr><% end_if %>
                    <% loop $getMetaDataList %>
                        <tr><th>$Key</th><td>$Value</td></tr>
                    <% end_loop %>
                </table>
            </div>
        </div>
    </div>
</section>

<% if $Description %>
    <div class="container">
        <section class="titled-section">
            <span class="section-title">Description</span>
            <div class="section-items">
                $Description
            </div>
        </section>
    </div>
<% end_if %>

<% if $SiteSettings.EnableReviews %>
    <section class="reviews-section">
        <div class="row responsive">
            <div class="c12">
                <strong class="title">Reviews</strong>
                <small class="sub-title">See what other customers had to say about <strong>$Title</strong>.</small>
                $ReviewSection()
                <small>Do you have experience about this product? Add your review and earn gift points towards your next purchase!</small>
            </div>
        </div>
    </section>
<% end_if %>

<% cached 'product_suggestions', $ThemeCacheID, $ID, $LastEdited, $Now.format('dMy') %>
<% if $getPeopleAlsoBought(12).count > 5 %>
    <section class="titled-section">
        <span class="section-title">People who bought this item also bought</span>
        <div class="section-items">
            <div class="owl-carousel carousel" data-slides="3" data-small="3" data-medium="4" data-large="6" data-nav="1">
                <% loop $getPeopleAlsoBought(12) %>
                    <div class="slide-owl-wrap">
                        <% include ProductCardSquare %>
                    </div>
                <% end_loop %>
            </div>
        </div>
    </section>
<% else_if $PeopleAlsoViewed(12).count > 5 %>
    <section class="titled-section">
        <span class="section-title">People who viewed this product also viewed</span>
        <div class="section-items">
            <div class="owl-carousel carousel" data-slides="3" data-small="3" data-medium="4" data-large="6" data-nav="1">
                <% loop $PeopleAlsoViewed(12) %>
                    <div class="slide-owl-wrap">
                        <% include ProductCardSquare %>
                    </div>
                <% end_loop %>
            </div>
        </div>
    </section>
<% end_if %>
<% if $YouMayAlsoLike(12).count > 5 %>
    <section class="titled-section">
        <span class="section-title">You may also like</span>
        <div class="section-items">
            <div class="owl-carousel carousel" data-slides="3" data-small="3" data-medium="4" data-large="6" data-nav="1">
                <% loop $YouMayAlsoLike(12) %>
                    <div class="slide-owl-wrap">
                        <% include ProductCardSquare %>
                    </div>
                <% end_loop %>
            </div>
        </div>
    </section>
<% end_if %>

<div class="lightbox ad" id="secure-payments">
    <div class="content">
        <a class="close-btn">X</a>

        <h2>Secure Payments</h2>
        <small>Online & Offline payment methods accepted</small>

        <p>&nbsp;</p>

        <p>
            <strong>Credit / Dedit Cards</strong><br />
            All major credits cards are accepted. <br />
            Transactions are processed by VivaWallet and a receipt is automatically issued upon payment.
        </p>

        <p>&nbsp;</p>

        <p>
            <strong>PayPal</strong><br />
            PayPal has gained a reputation as the most secure way to pay online.
        </p>

        <p>&nbsp;</p>

        <p>
            <strong>Cash on Delivery / Pickup</strong><br />
            Settle your bill upon receiving your order using either; Cash, Revolut or BOV Mobile.
        </p>
    </div>
</div>
<div class="lightbox ad" id="return-policy">
    <div class="content">
        <a class="close-btn">X</a>

        <h2>Return Policy</h2>
        <small>Not fully satisfied with your purchase? We're here to help.</small>

        <p>&nbsp;</p>

        <p>
            <strong>14-Day Money Back Guarantee</strong><br />
            You can return any item purchased on $SiteSettings.Title within 14-days to get a full refund.
        </p>

        <p>&nbsp;</p>

        <p>
            <strong>Terms and Conditions</strong><br />
            All returned items needs to be in their original packiging.<br />
            A copy of your receipt or order confirmation needs to be presented.<br />
            Damaged or visibly used items are non-returnable or refundable.
        </p>

        <p>&nbsp;</p>

        <p>
            <strong>It has been more then 14 days since your purchase?</strong><br />
            You may still contact our support team, where a credit note may be issued.<br />
            Please note; This is treated on a case by case basis.
        </p>
    </div>
</div>
<% end_cached %>